FROM python:3.10.13-slim
RUN apt-get update && apt-get install -y --no-install-recommends git

RUN cd /tmp && \
    git clone https://github.com/jcreinhold/intensity-normalization.git && \
    cd intensity-normalization && \
    python setup.py install && \
    cd && rm -rf /tmp/intensity-normalization

ENTRYPOINT ["fcm-normalize"]

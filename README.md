For full documentation, see: https://github.com/jcreinhold/intensity-normalization

Simple use-case for T1w images:
`fcm-normalize t1w_image.nii -m brain_mask.nii`

Advanced use-case for multi-contrast images.
1. `fcm-normalize t1w_image.nii.gz -m brain_mask.nii.gz -o t1w_norm.nii.gz -v -mo t1 -tt wm`
2. `fcm-normalize t2w_image.nii.gz -tm t1w_image_wm_membership.nii.gz -o t2w_norm.nii.gz -v -mo t2`

TODO: READ DOCS AND CONFIRM THESE COMMANDS ARE SUFFICIENT FOR ARBITRARY CONTRASTS
